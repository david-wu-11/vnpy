﻿# -*- coding=utf-8 -*-
# 官方网站：http://www.vnpy.cn
from ctypes import *
import os.path
class VNStrategy(Structure):
    _fields_ = [('StrategyFileName', c_char * 300), ('StrategyType', c_int)]
    pass

class StrategyBackTestProcess(object):
    '''
    def __init__(self,signal_md_tick):
        self.signal_md_tick = signal_md_tick
        currpath = os.path.abspath(os.path.dirname(__file__))
        self.vnmd = CDLL(currpath + '\\StrategyProcess.dll')
    '''



    def __init__(self,strategyfile):
        currpath = os.path.abspath(os.path.dirname(__file__))
        self.vnsp = CDLL(currpath + '\\VNStrategyProcess.dll')
        self.strategyfile = strategyfile
        print('构造策略类：'+self.strategyfile)
        #CMPFUNC = CFUNCTYPE(None, POINTER(VNStrategy))
        #self.vnsp.VNRegOnStrategyCalculate(CMPFUNC(self.OnStrategyCalculate))
    '''
    # 合约反订阅合约回调
    def OnStrategyCalculate(self, a):
        pass

    # 注册反订阅合约回调
    def VNRegOnStrategyCalculate(self):
        CMPFUNC = CFUNCTYPE(None, POINTER(VNStrategy))
        self.vnsp.VNRegOnStrategyCalculate(CMPFUNC(self.OnStrategyCalculate))
    '''

    def GetBackTestProcessHwnd(self):
        user32 = windll.user32
        hwnd = user32.GetForegroundWindow()
        print(hwnd)
        return hwnd