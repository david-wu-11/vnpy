﻿# -*- coding=utf-8 -*-
# 官方网站：http://www.vnpy.cn
from ctypes import *
import os.path
class vnfunctionapi(object):
    def __init__(self):
        currpath = os.path.abspath(os.path.dirname(__file__))
        self.dll = CDLL(currpath + '\\vnfunctionapi.dll')
        self.fAsynSleep = self.dll.AsynSleep
        self.fAsynSleep.argtypes = [c_int32]
    def AsynSleep(self, dwMilliseconds):
        self.fAsynSleep(dwMilliseconds)




