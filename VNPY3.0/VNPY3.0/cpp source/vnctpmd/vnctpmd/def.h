

#pragma once

//周期定义
#define VN_ALL   10000  	// 所有周期
#define VN_M1    10001    	// M1   1分钟
#define VN_M3    10002  	    // M3   3分钟
#define VN_M5    10003  	    // M5   5分钟
#define VN_M10   10004  	// M10  10分钟
#define VN_M15   10005  	// M15  15分钟
#define VN_M30   10006  	// M30  30分钟
#define VN_M60   10007  	// M60  60分钟
#define VN_M120  10008  	// M120 120分钟
#define VN_M240  10009  	// M120 120分钟
#define VN_D1    10010  	    // D1   1日



// PRICE TYPE
#define VN_CLOSE  10010   // 收盘价
#define VN_OPEN   10011   // 开盘价
#define VN_HIGH   10012   // 最高价
#define VN_LOW    10013   // 最低价


// PRICE TYPE
#define VN_MA     10014   // 收盘价
#define VN_RSI    10015   // 开盘价
#define VN_SAR    10016   // 最高价
#define VN_MACD   10017   // 最低价
#define VN_CCI    10018   // 最低价
#define VN_ATI    10019   // 最低价



#define  MAX_TICK_NUM   60//120//60  //基本数据，之取第一个下标位0的元素，数量无所谓
#define  MAX_TICK_MOVE_NUM   59


#define  MAX_PURE_TICK_PERMIN_NUM   120 // 240//120  //每分钟TICK数60X2  //TICK数不足120如何处理统计？



/*

#define  max_pure_tick_num   130  //保存120TICK的买卖量 的数量
#define  max_pure_tick_move_num   129  //保存120TICK的买卖量 的数量

#define  max_pure_m1_num          480  //每1分钟 120TICK
#define  max_pure_m1_move_num     479  //每1分钟 120TICK

#define  max_pure_m3_num          361  //160  //每3分钟 360TICK
#define  max_pure_m3_move_num     360  //159  //每3分钟 360TICK

#define  max_pure_m5_num          96  //每3分钟 360TICK
#define  max_pure_m5_move_num     95 //每3分钟 360TICK

#define  max_pure_m10_num          96  //每3分钟 360TICK
#define  max_pure_m10_move_num     95 //每3分钟 360TICK

#define  max_pure_m15_num          96  //每3分钟 360TICK
#define  max_pure_m15_move_num     95 //每3分钟 360TICK

#define  max_pure_m30_num          96  //每3分钟 360TICK
#define  max_pure_m30_move_num     95 //每3分钟 360TICK

#define  max_pure_m60_num          96  //每3分钟 360TICK
#define  max_pure_m60_move_num     95 //每3分钟 360TICK

#define  max_pure_m120_num          96  //每3分钟 360TICK
#define  max_pure_m120_move_num     95 //每3分钟 360TICK

#define  max_pure_m240_num          96  //每3分钟 360TICK
#define  max_pure_m240_move_num     95 //每3分钟 360TICK

#define  max_pure_d1_num          30  //每3分钟 360TICK
#define  max_pure_d1_move_num     29 //每3分钟 360TICK

#define  max_pure_m480_num        100  //按每天8小时估算为480分钟
#define  max_pure_m480_move_num   99  //按每天8小时估算为480分钟
*/


extern int max_pure_tick_num;// = 130;  //保存120TICK的买卖量 的数量
extern int max_pure_tick_move_num;// = 129; //保存120TICK的买卖量 的数量

extern int max_pure_m1_num;//= 480;  //每1分钟 120TICK
extern int max_pure_m1_move_num;// = 479;  //每1分钟 120TICK

extern int max_pure_m3_num;// = 361;  //160  //每3分钟 360TICK
extern int max_pure_m3_move_num;// = 360;  //159  //每3分钟 360TICK

extern int max_pure_m5_num;// = 96;  //每3分钟 360TICK
extern int max_pure_m5_move_num;// = 95; //每3分钟 360TICK

extern int max_pure_m10_num;// = 96;  //每3分钟 360TICK
extern int max_pure_m10_move_num;// = 95; //每3分钟 360TICK

extern int  max_pure_m15_num;//= 96;  //每3分钟 360TICK
extern int  max_pure_m15_move_num;// = 95; //每3分钟 360TICK

extern int  max_pure_m30_num;// = 96;  //每3分钟 360TICK
extern int  max_pure_m30_move_num;// = 95; //每3分钟 360TICK

extern int  max_pure_m60_num;// = 96; //每3分钟 360TICK
extern int  max_pure_m60_move_num;// = 95; //每3分钟 360TICK

extern int  max_pure_m120_num;// = 96;  //每3分钟 360TICK
extern int  max_pure_m120_move_num;//= 95; //每3分钟 360TICK

extern int  max_pure_m240_num;//= 96;  //每3分钟 360TICK
extern int  max_pure_m240_move_num;// = 95;//每3分钟 360TICK

extern int  max_pure_d1_num;// = 30;  //每3分钟 360TICK
extern int  max_pure_d1_move_num;// = 29; //每3分钟 360TICK

extern int  max_pure_m480_num;// = 100;  //按每天8小时估算为480分钟
extern int  max_pure_m480_move_num;// = 99;  //按每天8小时估算为480分钟


//extern char	InstrumentID_n[TYPE_NUM][10];

#define MAX_USERNAME 33   //15
#define MAX_TRY_NUMBER 5  //5
#define MAX_ADDR_NUMBER 5
#define MAX_PACKET_SIZE 1024
#define SERVER_PORT 4567

#define  MAX_CROSSSINGLE_NUM   500
#define  MAX_CROSSSINGLE_MOVE_NUM   499
#define TYPE_NUM   20

 


#define TYPE_ARR_LEN 60  //应该等于所有品种宏的数量100000~1000059

#define  TYPE_NI   100000 //移位的下标开始
#define  TYPE_ZN   100001 //移位的下标开始
#define  TYPE_AL   100002 //移位的下标开始
#define  TYPE_CU   100003 //移位的下标开始
#define  TYPE_AU   100004 //移位的下标开始
#define  TYPE_AG   100005 //移位的下标开始
#define  TYPE_I    100006 //移位的下标开始
#define  TYPE_RU   100007 //移位的下标开始
#define  TYPE_TA   100008 //移位的下标开始
#define  TYPE_A    100009 //移位的下标开始
#define  TYPE_M    100010 //移位的下标开始
#define  TYPE_Y    100011 //移位的下标开始
#define  TYPE_P    100012 //移位的下标开始
#define  TYPE_RB   100013 //移位的下标开始
#define  TYPE_MA   100014 //移位的下标开始
#define  TYPE_PP   100015 //移位的下标开始
#define  TYPE_CS   100016 //移位的下标开始
#define  TYPE_JD   100017 //移位的下标开始
#define  TYPE_BU   100018 //移位的下标开始
#define  TYPE_FG   100019 //移位的下标开始
#define  TYPE_L    100020 //移位的下标开始
#define  TYPE_V    100021 //移位的下标开始
#define  TYPE_J    100022 //移位的下标开始

#define  TYPE_SR   100023 //移位的下标开始
#define  TYPE_RM   100024 //移位的下标开始
#define  TYPE_CF   100025 //移位的下标开始

#define  TYPE_C    100026 //移位的下标开始
#define  TYPE_WH   100027 //移位的下标开始
#define  TYPE_SM   100028 //移位的下标开始
#define  TYPE_SF   100029 //移位的下标开始


//增加的
#define  TYPE_PB   100030 //移位的下标开始
#define  TYPE_SN   100031 //移位的下标开始
#define  TYPE_WR   100032 //移位的下标开始
#define  TYPE_HC   100033 //移位的下标开始
#define  TYPE_FU   100034 //移位的下标开始
//增加的



#define  TYPE_IC   100035 //移位的下标开始
#define  TYPE_IF   100036 //移位的下标开始
#define  TYPE_IH   100037 //移位的下标开始

#define  TYPE_T    100038 //十国债
#define  TYPE_TF   100039 //五国债

#define  TYPE_NEW1    100040 
#define  TYPE_NEW2    100041
#define  TYPE_NEW3    100042
#define  TYPE_NEW4    100043
#define  TYPE_NEW5    100044
#define  TYPE_NEW6    100045
#define  TYPE_NEW7    100046
#define  TYPE_NEW8    100047
#define  TYPE_NEW9    100048
#define  TYPE_NEW10   100049
#define  TYPE_NEW11   100050
#define  TYPE_NEW12   100051 
#define  TYPE_NEW13   100052
#define  TYPE_NEW14   100053
#define  TYPE_NEW15   100054
#define  TYPE_NEW16   100055
#define  TYPE_NEW17   100056
#define  TYPE_NEW18   100057
#define  TYPE_NEW19   100058
#define  TYPE_NEW20   100059



 
#include <vector>
using namespace std;
struct ZQData
{
	double Open;
	double Close;
	double High;
	double Low;
	double AveragePrice;
	double HighestPrice;
	double LowestPrice;
	double ZQTime;
	char TradeingDay;
};

struct MarketdataSource                      
{
	int key;

	char InstrumentID[10];
	int  TYPEID;
	char	TickFileWritepaths[20];
	int  tick_VolumeLast;
	vector<ZQData> Data_M1;
	vector<ZQData> Data_M3;
	vector<ZQData> Data_M5;
	vector<ZQData> Data_M10;
	vector<ZQData> Data_M15;
	vector<ZQData> Data_M30;
	vector<ZQData> Data_M60;
	vector<ZQData> Data_M120;
	vector<ZQData> Data_D1;

  
 

	
};
#pragma pack()